var gulp = require('gulp'),
	connect = require('gulp-connect'),
	sass = require('gulp-sass'),
	cssmin = require("gulp-cssmin"),
	runSequence = require('run-sequence'),
	rename = require('gulp-rename'),
	del = require('del'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	jshint = require('gulp-jshint'),
	cache = require('gulp-cache'),
	changed = require('gulp-changed'),
	autoprefixer = require('gulp-autoprefixer'),
	imagemin = require('gulp-imagemin');

var paths = {
	src: './src/',
	dest: './build/',
	fonts: [
		'./bower_components/Materialize/fonts/**/*',
		'./src/assets/fonts/**/*'
		],
	includePaths: [
		'./bower_components/Materialize/sass/'
		]
};

/* vendor Scripts */
var vendor = {
    scripts: [
        './bower_components/jquery/dist/jquery.min.js',
        './bower_components/materialize/dist/js/materialize.min.js',
        // './bower_components/d3/d3.min.js'
        ],
    styles: [
        // 'app/bower_components/nvd3/build/nv.d3.min.css',
        // 'app/bower_components/dragular/dist/dragular.min.css'
        ]
};

// 1. Servidor web de desarrollo
gulp.task('server', function() {
    return connect.server({
        root: paths.dest,
        hostname: '0.0.0.0',
        port: 8888,
        livereload: true
    });
});


gulp.task('vendorJs', function() {
    return gulp.src(vendor.scripts)
            .pipe(concat('vendor.js'))
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest(paths.dest+'assets/js'))
});

gulp.task('vendorCss', function() {
    return gulp.src(vendor.styles)
	        .pipe(concat('vendor.css'))
	        .pipe(cssmin())
	        .pipe(rename({suffix: '.min'}))
	        .pipe(gulp.dest(paths.dest+'assets/css'))
});

gulp.task('js', function() {
    return  gulp.src([paths.src+'js/**/*.js'])
                .pipe(jshint())
                .pipe(jshint.reporter('default'))
                .pipe(concat('index.js'))
                .pipe(gulp.dest(paths.dest+'assets/js'))
                .pipe(uglify())
                .pipe(rename({suffix: '.min'}))
                .pipe(gulp.dest(paths.dest+'assets/js'))
                .pipe(connect.reload());
});

gulp.task('sass', function () {
    return gulp.src([paths.src+'sass/main.scss'])
    	.pipe(changed('./app/assets/css'))
        .pipe(sass({includePaths : paths.includePaths }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulp.dest(paths.dest+'assets/css'))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.dest+'assets/css'))
        .pipe(connect.reload());
});

gulp.task('images',function() {
	return gulp.src(paths.src+'img/**/*')
		.pipe(cache(imagemin({quality: 90,optimizationLevel: 3, progressive: true, interlaced: true})))
		.pipe(gulp.dest(paths.dest+'assets/img/'));
});

//HTML pages
gulp.task('html', function() {
	return gulp.src(paths.src+'**/*.html')
	    .pipe(gulp.dest(paths.dest));
});

// copy required fonts to dest
gulp.task('fonts', function () {
    return gulp
        .src(paths.fonts)
        .pipe(gulp.dest(paths.dest+'assets/fonts/'));
});

gulp.task('watch', function() {
    gulp.watch([paths.src+'js/**/*.js'], ['js']);
    gulp.watch(paths.src+'sass/**/*.scss', ['sass']);
    gulp.watch(paths.src+'**/*.html', ['html']);
});

gulp.task('default',function() {
    return runSequence('vendorJs','js','sass','images',['html','fonts'],'server', 'watch');
});