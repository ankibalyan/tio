// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.

(function ($) {
  $(document).ready(function() {

    $(document).on('mouseover.card', '.card', function (e) {
      if ($(this).find('> .card-reveal').length) {
        $(e.target).closest('.card').css('overflow', 'hidden');
        $(this).find('.card-reveal').css({ display: 'block'}).velocity("stop", false).velocity({translateY: '-100%'}, {duration: 300, queue: false, easing: 'easeInOutQuad'});
      }

      $('.card-reveal').closest('.card').css('overflow', 'hidden');

    });
    $(document).on('mouseout.card', '.card', function (e) {
      if ($(this).find('> .card-reveal').length) {
        // Make Reveal animate down and display none
        $(this).find('.card-reveal').velocity(
            {translateY: 0}, {
              duration: 225,
              queue: false,
              easing: 'easeInOutQuad',
              complete: function() { $(this).css({ display: 'none'}); }
            }
        );
      }

      $('.card-reveal').closest('.card').css('overflow', 'hidden');

    });
  });
}( jQuery ));